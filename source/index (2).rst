.. Basis documentation master file, created by
   sphinx-quickstart on Mon Nov 29 09:05:20 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Basis documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview

   tutorials

   how-to-guides

   explanation

   reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
