import json
import yaml
import pprint

def pp ( obj="", apply_sort=False ):
   pprint.pprint ( obj, indent=3, width=132, sort_dicts=apply_sort )

def pf ( obj="", apply_sort=False ):
   return pprint.pformat ( obj, indent=3, width=132, sort_dicts=apply_sort )


with open('./small.yaml') as f:
   y = yaml.safe_load ( f )
   ans = json.dumps ( y )


lines = []
with open('./small.yaml') as f:
   for l in f.readlines():
      lines.append ( l.rstrip() )
      ans = "%0A".join ( lines )
   
