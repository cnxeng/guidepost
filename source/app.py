import json
import yaml
import pprint
import http.client
import urllib.parse
import requests

from flask      import Flask
from flask      import request
from flask_cors import CORS

from markupsafe import escape

from flasgger   import Swagger
from flasgger   import swag_from

YOUR_ACCESS_KEY = "4f41e6b0ba0d1c092c13661fc7a0f431"


# create the Flask app
app = Flask ( __name__ )
#CORS ( app )
swagger = Swagger(app)


def pp ( obj="", apply_sort=False ):
   pprint.pprint ( obj, indent=3, width=132, sort_dicts=apply_sort )

def pf ( obj="", apply_sort=False ):
   return pprint.pformat ( obj, indent=3, width=132, sort_dicts=apply_sort )

@app.route('/')
def hello_world():
   return "hello, CORS world!\n"





#####################################################################################################
#####################################################################################################
#####################################################################################################
###
###  https://flask.palletsprojects.com/en/2.0.x/quickstart/#routing
###


@app.route('/user/<username>')
def show_user_profile(username):
   # show the user profile for that user
   return f"User: {escape(username)}\n"

@app.route('/post/<int:post_id>')
def show_post(post_id):
   # show the post with the given id, the id is an integer
   return f"Post: {post_id}\n"

@app.route('/path/<path:subpath>')
def show_subpath(subpath):
   # show the subpath after /path/
   return f"Subpath: {escape(subpath)}\n"


###
###
###
#####################################################################################################
#####################################################################################################
#####################################################################################################





#####################################################################################################
#####################################################################################################
#####################################################################################################
###
###  https://www.digitalocean.com/community/tutorials/processing-incoming-request-data-in-flask
###


@app.route ( "/query-example" )
def query_example():
   lang = request.args.get ( 'language' )
   return f"Query String Example: {language=}\n"


@app.route ( "/form-example", methods=[ "GET", "POST" ] )
def form_example():
#   return 'Form Data Example'

   ans = ""

   if request.method == "GET":
      ans = '''
      <form method="POST">
      <div><label>Language: <input type="text" name="language"></label></div>
      <div><label>Framework: <input type="text" name="framework"></label></div>
      <input type="submit" value="Submit">
      </form>'''

   elif request.method == 'POST':
      language  = request.form.get ( "language"  )
      framework = request.form.get ( "framework" )
      ans = '''
      <h1>The language value is: {}</h1>
      <h1>The framework value is: {}</h1>'''.format ( language, framework )

   else:
      ans = "????"

   return ans+"\n"


@app.route ( "/json-example", methods=[ "POST" ] )
def json_example():
   ans  = "JSON Object Example"
   ans += "\n"
   ans += "%%%%%%%%%%%%%%%%%%%"
   ans += "\n"
   ans += pprint.pformat ( request.get_json(), indent=3, width=1024, sort_dicts=False )
   ans += "\n"

   return ans


###
###
###
#####################################################################################################
#####################################################################################################
#####################################################################################################





#####################################################################################################
#####################################################################################################
#####################################################################################################
###
###  https://positionstack.com/documentation
###


@app.route ( "/forward" )
def geocode_forward():
   ans  = ""

#   conn = http.client.HTTPConnection ( "api.positionstack.com" )
#
#   params = urllib.parse.urlencode ({
#      'access_key': YOUR_ACCESS_KEY,
#      'query'     : "Copacabana",
#      'region'    : "Rio de Janeiro",
#      'limit'     : 1,
#   })
#
#   conn.request ( "GET", "/v1/forward?{}".format ( params ) )
#
#   res  = conn.getresponse()
#   data = res.read()
#
#   ans  = pprint.pformat ( data.decode('utf-8'), indent=3, width=1024, sort_dicts=False )
#   ans += "\n"

#   params = {
#      'access_key': YOUR_ACCESS_KEY,
#      'query'     : "Copacabana",
#      'region'    : "Rio de Janeiro",
#      'limit'     : 1,
#   }
   params = {
      'access_key': YOUR_ACCESS_KEY,
      'query'     : request.args['query'],
      'limit'     : request.args.get ( 'limit', 3 ),
   }
   r = requests.get ( "http://api.positionstack.com/v1/forward", params=params )
   ans  = pf ( r.json(), apply_sort=False )
   ans += "\n"

   return ans


@app.route ( "/reverse" )
def geocode_reverse():
   ans  = ""

#   conn = http.client.HTTPConnection ( "api.positionstack.com" )
#
#   params = urllib.parse.urlencode ({
#      'access_key': YOUR_ACCESS_KEY,
#      'query'     : "51.507822,-0.076702",
#   })
#
#   conn.request ( "GET", "/v1/reverse?{}".format ( params ) )
#
#   res  = conn.getresponse()
#   data = res.read()
#
#   ans  = pprint.pformat ( data.decode('utf-8'), indent=3, width=1024, sort_dicts=False )
#   ans += "\n"

#   params = {
#      'access_key': YOUR_ACCESS_KEY,
#      'query'     : "51.507822,-0.076702",
#   }
   params = {
      'access_key': YOUR_ACCESS_KEY,
      'query'     : request.args['query'],
      'limit'     : request.args.get ( "limit", 3 ),
   }
   r = requests.get ( "http://api.positionstack.com/v1/reverse", params=params )
   ans  = pf ( r.json(), apply_sort=False )
   ans += "\n"

   return ans


@app.route ( "/openapi" )
def fetch_openapi():
#   ans = ""
#   y = yaml.safe_load ( "http://127.0.0.1:5000/openapi.yaml" )
#   ans = json.dumps ( y )
#   return ans
   return "sfsg"



###
###
###
#####################################################################################################
#####################################################################################################
#####################################################################################################



#
# ?> python flask_app.py
#


if __name__ == '__main__':
#   # run app in debug mode on port 5000
#   app.run(debug=True, port=5000)
   app.run(debug=True, port=5000)
