Overview
========

what it is all about

nice graphic: https://documentation.divio.com/, in the section 'About the system'

note: there is a great talk out on `youtube <https://www.writethedocs.org/videos/eu/2017/the-four-kinds-of-documentation-and-why-you-need-to-understand-what-they-are-daniele-procida/>`_

also: :download:`see this PDF file <_static/LogWatch.pdf>`.

plus: view this HTML: `guide for BVT <_static/extract.html>`_
